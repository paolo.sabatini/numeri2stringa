# Numeri2stringa

programma che legge un numero in input e ne stampa la sua rappresentazione in lettere.

Esempio , input 323 output trecentoventitre 


soluzione "immediata" :

se, ad esempio, i numeri in input ammissibili sono i numeri da 1 a 10 la soluzione immediata sarebbe quella di creare un array di stringhe inizializzato come :

inLettere[0]="zero";
inLettere[1]="uno";
...
inLettere[10]="dieci";


Ma se fosse per un intervallo fino a 100 o a 1000 ??

potremmo sfruttare le caratteristiche della "ricorrenza" esempio centouno centodue centotre ecc...
oppure sfruttare anche la soluzione immediata ma ci toccherebbe scrivere un sacco di righe, ma siamo in tanti e git/gitLab ci da una mano ...
ci potremmo dividere il lavoro!!
